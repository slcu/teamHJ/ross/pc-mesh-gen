"""A setuptools based setup module.
See:
https://packaging.python.org/guides/distributing-packages-using-setuptools/
https://github.com/pypa/sampleproject
"""

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
import pathlib

here = pathlib.Path(__file__).parent.resolve()

# Get the long description from the README file
long_description = (here / 'README.md').read_text(encoding='utf-8')

# Arguments marked as "Required" below must be included for upload to PyPI.
# Fields marked as "Optional" may be commented out.

setup(
    name='pc_mesh_generation',  # Required
    version='0.0.1',  # Required
    description='Code for generating tissue meshes from confocal images',  # Optional
    long_description=long_description,  # Optional
    long_description_content_type='text/markdown',  # Optional (see note above)
    url='https://gitlab.com/slcu/teamHJ/ross/pc-mesh-gen',  # Optional
    author='Ross Carter',  # Optional
    author_email='ross.carter@slcu.cam.ac.uk',  
    packages=find_packages(where='pc_mesh_gen'),  # Required
    python_requires='>=3.6, <4',
    install_requires=['matplotlib',
                      'tqdm',
                      'numpy',
                      'pillow',
                      'scikit-image',
                    #   'pyacvd',
                      'pyvista'
                      ],

    entry_points={  # Optional
        'console_scripts': [
            'mesh_all = pc_mesh_gen.command_line:mesh_all',
        ],
    },

)