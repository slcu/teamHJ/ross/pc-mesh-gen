#!/usr/bin/env bash

# ${1} is .init file to append info to
# ${2} is number of times to append, ie number of cells in init file

# add 35 cell data entries 

# get number of cells
read -r first_line < ${1}
IFS=' ' # space is set as delimiter
read -ra a <<< "$first_line"
num_cells=$a
echo ${num_cells}


# remove and replace last line
tail -n 1 "${1}" | wc -c | xargs -I {} truncate "${1}" -s -{}
echo "${num_cells} 35" >> ${1}

for ((i=0; i<=$num_cells; i++))
do
    echo "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0" >> ${1}
done
